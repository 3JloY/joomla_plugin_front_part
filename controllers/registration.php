<?php
// Запрет прямого доступа.
defined('_JEXEC') or die;
 
// Подключаем библиотеку controlleradmin Joomla.
jimport('joomla.application.component.controlleradmin');
 
/**
 * HelloWorlds контроллер.
 */
class CongressControllerRegistration extends JControllerLegacy
{
  /**
   * Прокси метод для getModel.
   *
   * @param   string  $name    Имя класса модели.
   * @param   string  $prefix  Префикс класса модели.
   *
   * @return  object  Объект модели.
   */
  public function getModel($name = 'registration', $prefix = 'CongressModel') {
    return parent::getModel($name, $prefix, array('ignore_request' => true));
  }

  public function add() {
    $model = $this->getModel();
    $inputCookie  = JFactory::getApplication()->input->cookie;
   
    $data = (JRequest::get('request')) ;

    $con_id = (empty($data['con_id'])) ? $inputCookie->get('con_id') : $data['con_id'];

    $inputCookie->set('con_id', $con_id, 0);
    $user = JFactory::getUser();

    $profile = JUserHelper::getProfile($user->id);
    
    if ($user->guest) {
      JFactory::getApplication()->enqueueMessage('Для регистрации на конгрессе выполните вход на сайт!');
      $this->setRedirect("index.php?option=com_congress&layout=login");
    }else {
      if ($this->check_profile($user)) {
        if ($model->member_reg($user, $con_id)) {
          // Оплатить участие в конрессе вы можете в личном кабинете или на странице со списком конгресов нажав кноку оплатить!
          $this->setRedirect(JRoute::_('index.php?option=com_congress'));
        } else {
          $this->setRedirect(JRoute::_('index.php?option=com_congress'));
        }
      }else {
        JFactory::getApplication()->enqueueMessage('Для регистрации на конгрессе нам нужна дополнительная информация!');
        $this->setRedirect(JRoute::_("index.php?option=com_congress&layout=editprofile"));
      }
    }
  }

  public function check() {
    $model = $this->getModel();
    $model->check_payment();
  }

  public function pay() {
    $model = $this->getModel();
    $inputCookie  = JFactory::getApplication()->input->cookie;
    
    $data = (JRequest::get('post')) ;
    
    $con_id = (empty($data['con_id'])) ? $inputCookie->get('con_id') : $data['con_id'];

    $inputCookie->set('con_id', $con_id, 0);
    $user = JFactory::getUser();
    $profile = JUserHelper::getProfile($user->id);
    
    if ($user->guest) {
      $this->setRedirect("index.php?option=com_congress&layout=login");
    } else {
      if ($this->check_profile($user)) {
        if ($model->member_pay($user, $con_id)) {
          $this->setRedirect(JRoute::_('index.php?option=com_congress'));
        } else {
          $this->setRedirect(JRoute::_('index.php?option=com_congress'));
        }
      }
    }
  }

  public function login() {
    $model = $this->getModel('Registration', 'CongressModel');
    $data = JRequest::get('post');
    $app = JFactory::getApplication();
    
    $inputCookie  = $app->input->cookie;
    $con_id        = $inputCookie->get('con_id');
    if ($model->get_user_by_username($data['username'])) {
      $error = $app->login($data);
      $user = JFactory::getUser();
      // if ($this->check_profile($user)) {
        $this->add();
      // }
    } else {
      JFactory::getApplication()->enqueueMessage('Данного пользователя не найдено! Пожалуйста зарегестрируйтесь.
        После регистрации на сайте повторите регистрацию на конгерссе!');
      $this->setRedirect(JRoute::_('index.php?option=com_users&view=registration'));
      // $model->save($data);
    }
  }

  /**
   * check_profile function to check currently fill user profile
   * @param  object JUser $user current user
   * @return redirect to edit profile or return true
   */
  public function check_profile($user) {
    $profile = JUserHelper::getProfile($user->id);

    $profile = $profile->profile;
    
    unset($profile['dob']);
    
    if (empty($profile)) {
      return false;
      $this->setRedirect(JRoute::_("index.php?option=com_congress&layout=editprofile"));
    }
    
    foreach ($profile as $attr) {
      if (empty($attr)) {
        return false;
        $this->setRedirect(JRoute::_("index.php?option=com_congress&layout=editprofile"));
      }
    }
    return true;
  }

  public function edit_profile() {
    
    $app  = JFactory::getApplication();
    $model  = $this->getModel('Registration', 'CongressModel');
    $user = JFactory::getUser();
    $userId = (int) $user->get('id');

    // Get the user data.
    $data = $app->input->post->get('jform', array(), 'array');
    $data['id'] = $userId;

    $return = $model->save($data);
    
    if ($return) {
      $this->add();
    }
  }
}