<?php
// Запрет прямого доступа.
defined('_JEXEC') or die;

 
// Подключаем библиотеку представления Joomla.
jimport('joomla.application.component.view');
 
/**
 * HTML представление списка сообщений компонента HelloWorld.
 */
class CongressViewCongresses extends JViewLegacy {
  
  /**
   * Сообщения.
   *
   * @var  array 
   */
  protected $items;

  /**
   * Постраничная навигация.
   *
   * @var  object
   */
  protected $pagination;

  /**
   * Отображаем список сообщений.
   *
   * @param   string  $tpl  Имя файла шаблона.
   *
   * @return  void
   *
   * @throws  Exception
   */
  public function display($tpl = null) {
    try { 
      $app = JFactory::getApplication();
      $this->model = $this->getModel();
      // Текущий пользователь
      $this->user = JFactory::getUser();
      // Получаем данные из модели.
      $this->items = $this->get('Items');
  
      $this->setLayout($app->input->get('layout'));
      
      if ('editprofile' == $app->input->get('layout')) {
        $profile = JUserHelper::getProfile($this->user->id);
        $this->profile = $profile->profile;
      }



      $this->current_date = strtotime(date("Y-m-d"));
      
      // Получаем объект постраничной навигации.
      $this->pagination = $this->get('Pagination');
      // Отображаем представление.
      parent::display($tpl);
    }
    catch (Exception $e)
    {
        throw new Exception($e->getMessage());
    }
  }
}