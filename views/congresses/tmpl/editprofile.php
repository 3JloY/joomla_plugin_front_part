<?php 

  defined('_JEXEC') or die;

  JHtml::_('behavior.keepalive');
  JHtml::_('behavior.formvalidation');
  JHtml::_('formbehavior.chosen', 'select');

 ?>
<div id="congresses" >
  <form id="member-profile" action="<?php echo JRoute::_('index.php?option=com_congress&task=registration.edit_profile'); ?>" method="post" class="form-validate form-horizontal" enctype="multipart/form-data">
    <fieldset>
      
      <h2>Дополнительная информация</h2>
      
      <div class="control-group">
        <div class="controls">
          <input placeholder="Адрес 1" type="text" name="jform[profile][address1]" id="jform_profile_address1" value="<?php echo $this->profile['address1'] ?>" size="30" required="" aria-required="true">
        </div>
      </div>
      
      <div class="control-group">
        <div class="controls">
          <input placeholder="Город" type="text" name="jform[profile][city]" id="jform_profile_city" value="<?php echo $this->profile['city'] ?>" size="30" required="" aria-required="true">
        </div>
      </div>
      
      <div class="control-group">
        <div class="controls">
          <input placeholder="Регион" type="text" name="jform[profile][region]" id="jform_profile_region" value="<?php echo $this->profile['region'] ?>" size="30" required="" aria-required="true">
        </div>
      </div>

      <div class="control-group">
        <div class="controls">
          <input placeholder="Страна" type="text" name="jform[profile][country]" id="jform_profile_country" value="<?php echo $this->profile['country'] ?>" size="30" required="" aria-required="true">
        </div>
      </div>
      
      <div class="control-group">
        <div class="controls">
          <input placeholder="Почтовый индекс" type="text" name="jform[profile][postal_code]" id="jform_profile_postal_code" value="<?php echo $this->profile['postal_code'] ?>" size="30" required="" aria-required="true">
        </div>
      </div>

      <div class="control-group">
        <div class="controls">
          <input placeholder="Телефон" type="tel" name="jform[profile][phone]" id="jform_profile_phone" value="<?php echo $this->profile['phone'] ?>" size="30" required="" aria-required="true">
        </div>
      </div>
    </fieldset>

    <input type="submit" value="Сохранить" class="btn">
  </form>
</div>