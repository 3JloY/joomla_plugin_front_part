<?php
// Запрет прямого доступа.
defined('_JEXEC') or die;
foreach ($this->items as $i => $item) : ?>
  <?php $current_price = ($this->current_date >= strtotime($item->deadline_first_price)) ? $item->second_price : $item->price ; ?>
  <?php $register = $this->model->getRegisterByArgs($item->id, $this->user->id) ?>
  <tr>
    <td>
      <?php echo $item->title; ?>
    </td>
    <td>
      <?php echo $current_price; ?>
    </td>
    <td>
      <?php echo $item->deadline_registration; ?>
    </td>
    
    <?php if ( $this->current_date >= strtotime($item->deadline_registration) ): ?>
      <td>
        <?php // echo JText::_('COM_CONGRESS_CONGRESS_REGISTRATION_CLOSED'); ?>
        <?php echo JText::_('Регистрация закрыта'); ?>
      </td>
    <?php else : ?>
      <td>
        <?php if(!$register) : ?>
          <form action="<?php echo JRoute::_('index.php?option=com_congress&task=registration.add'); ?>" method="post" name="adminForm" id="adminForm">
            <input class='reg-button' type="submit" value="Принять участие" name="register">
            <input type="hidden" name="con_id" value="<?php echo $item->id; ?>">
          </form>
        <?php else: ?>
          <?php echo JText::_('Вы уже зарегестрировались'); ?>
        <?php endif; ?>
        
        <?php if(false && $register && !$register->payment): ?>
          <form action="<?php echo JRoute::_('index.php?option=com_congress&task=registration.pay'); ?>" method="post" name="adminForm" id="adminForm">
            <input type="submit" value="Оплатить" name="payment">
            <input type="hidden" name="con_id" value="<?php echo $item->id; ?>">
          </form>
        <?php elseif($register->payment) : ?>
          <?php // echo JText::_('Вы уже зарегестрировались и произвели оплату'); ?>
          <?php echo JText::_('Вы уже произвели оплату'); ?>
        <?php endif; ?>

      </td>
    <?php endif ?>

  </tr>
<?php endforeach; ?>