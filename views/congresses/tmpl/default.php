<?php
// Запрет прямого доступа.
defined('_JEXEC') or die;
 
// Загружаем тултипы.
JHtml::_('bootstrap.tooltip');
?>
<div id="congresses">
  <table id="congresses-table" class="table table-striped" width="100%">
    <thead><?php echo $this->loadTemplate('head');?></thead>
    <tbody><?php echo $this->loadTemplate('body');?></tbody>
    <?php if ($this->pagination->total > 1) : ?>
      <tfoot><?php echo $this->loadTemplate('foot');?></tfoot>
    <?php endif; ?>
  </table>

  <div>
    <?php echo JHtml::_('form.token'); ?>
  </div>
</div>