<?php
  // Запрет прямого доступа.
  defined('_JEXEC') or die;

  require_once JPATH_COMPONENT.'/helpers/YandexNotification.php';
  
  // Подключаем логирование.
  JLog::addLogger( array('text_file' => 'com_congress.php'), 
    JLog::ALL, 
    array('com_congress') );
  
  // Устанавливаем обработку ошибок в режим использования Exception.
  JError::$legacy = false;
   
  // Подключаем библиотеку контроллера Joomla.
  jimport('joomla.application.component.controller');
   
  // Получаем экземпляр контроллера с префиксом Congress.
  $controller = JControllerLegacy::getInstance('congress');

  // Исполняем задачу task из Запроса.
  $input = JFactory::getApplication()->input;
  $controller->execute($input->getCmd('task', 'display'));
   
  // Перенаправляем, если перенаправление установлено в контроллере.
  $controller->redirect();