<?php
// Запрет прямого доступа.
defined('_JEXEC') or die;
 
// Подключаем библиотеку modellist Joomla.
jimport('joomla.application.component.modellist');
 
/**
 * Модель списка сообщений компонента Congress.
 */
class CongressModelCongresses extends JModelList {
  /**
   * Метод для построения SQL запроса для загрузки списка данных.
   *
   * @return  string  SQL запрос.
   */
  protected function getListQuery() {
    // Создаем новый query объект.
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);

    // Выбераем поля.
    $query->select('uc.id, uc.title, uc.price, uc.second_price, uc.deadline_first_price, uc.deadline_registration');
    // $query->select('uc.id, uc.title, uc.price, uc.second_price, uc.deadline_first_price, uc.deadline_registration, ur.user_id, ur.payment');

    // Из таблицы helloworld.
    $query->from('#__uror_congressesn AS uc');
    
    // инклюдим таблицу регистрации
    // $query->leftJoin('#__uror_registrationsn AS ur ON uc.id = ur.congress_id');

    return $query;
  }

  public function getRegisterByCongressId($congress_id) {
    // Создаем новый query объект.
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);

    // Выбераем поля.
    $query->select('*');

    // Из таблицы helloworld.
    $query->from('#__uror_registrationsn AS uc');
    $query->where($db->quoteName('congress_id') . ' = '. $db->quote($congress_id) );

    // Reset the query using our newly populated query object.
    $db->setQuery($query);
     
    // Load the results as a list of stdClass objects (see later for more options on retrieving data).
    $results = $db->loadObjectList();

    return $results;
  }

  public function getRegisterByUserId($user_id) {
    // Создаем новый query объект.
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);

    // Выбераем поля.
    $query->select('*');

    // Из таблицы helloworld.
    $query->from('#__uror_registrationsn AS uc');
    $query->where($db->quoteName('user_id') . ' = '. $db->quote($user_id) );

    // Reset the query using our newly populated query object.
    $db->setQuery($query);
     
    // Load the results as a list of stdClass objects (see later for more options on retrieving data).
    $results = $db->loadObjectList();

    return $results;
  }

  public function getRegisterByArgs($congress_id, $user_id) {
    
    // Создаем новый query объект.
    $db = JFactory::getDbo();
    $query = $db->getQuery(true);

    // Выбераем поля.
    $query->select('*');

    // Из таблицы helloworld.
    $query->from('#__uror_registrationsn AS uc');
    $query->where($db->quoteName('congress_id') . ' = '. $db->quote($congress_id) . 'AND' . $db->quoteName('user_id') . ' = '. $db->quote($user_id) );

    // Reset the query using our newly populated query object.
    $db->setQuery($query);
     
    // Load the results as a list of stdClass objects (see later for more options on retrieving data).
    $results = $db->loadObject();

    return $results;
  }

}