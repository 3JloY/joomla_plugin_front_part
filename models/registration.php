<?php
// Запрет прямого доступа.
defined('_JEXEC') or die;
 
// Подключаем библиотеку modelitem Joomla.
jimport('joomla.application.component.modelitem');
 
/**
 * Модель сообщения компонента HelloWorld.
 */
class CongressModelRegistration extends JModelItem {

  /**
   * Возвращает ссылку на объект таблицы.
   *
   * @param   string  $type    Тип таблицы.
   * @param   string  $prefix  Префикс имени класса таблицы. Необязателен.
   * @param   array   $config  Конифгурационный массив для таблицы. Необязателен.
   *
   * @return  JTable  Объект таблицы.
   */
  public function getTable($type = 'Registration', $prefix = 'CongressTable', $config = array()) {
    return JTable::getInstance($type, $prefix, $config);
  }

  public function register($data) {
    $table = $this->getTable();
    return true;
  }

  public function get_user_by_username($username) {

    if (!isset($username) || empty($username))
      return false;

    $db = $this->getDbo();
    $query = $db->getQuery(true);

    $query->select($db->quoteName('id'));
    $query->from($db->quoteName('#__users'));
    $query->where($db->quoteName('username') . ' = "' . $username . '"');

    $db->setQuery($query);
    if ($user = $db->loadRow()) {
      return $user;
    } else {
      return false;
    }
  }

  /**
   * Метод для авто-заполнения состояния модели.
   *
   * Заметка. Вызов метода getState в этом методе приведет к рекурсии.
   *
   * @return  void
   */
  protected function populateState() {
    parent::populateState();
  }

  public function save($data) {

    $userId = (!empty($data['id'])) ? $data['id'] : (int) $this->getState('user.id');
    
    $user = new JUser($userId);

    $new_user['name']     = $user->name;
    $new_user['username']     = $user->username;
    $new_user['profile'] = $data['profile'];

    // Load the users plugin group.
    // JPluginHelper::importPlugin('user');

    // echo "<pre>";
    $user->bind($data);
    
    return $user->save();
    // die();
    
    // $user->bind($data);
  }

  public function member_reg($user, $con_id) {
    $table = $this->getTable();
    $cmodel = JModelLegacy::getInstance('Congresses', 'CongressModel');
    $reg_row = $cmodel->getRegisterByArgs($con_id, $user->id);

    $data = array(
      'user_id' => $user->id,
      'congress_id' => $con_id
    );

    if ($this->check_user_registration($user->id)) {
      JFactory::getApplication()->enqueueMessage('Вы уже зарегестрированны на конгресс!');
      var_dump($reg_row->payment);
      if (!isset($reg_row->payment)) {
        // JFactory::getApplication()->enqueueMessage('Оплатить участие в конрессе вы можете в личном кабинете или на странице со списком конгресов нажав кноку оплатить!');
      }
      return true;
    }

    if (!$table->bind($data)) {
      JFactory::getApplication()->enqueueMessage('Извините произошла ошибка', 'error');
      return false;
    }

    if (!$table->store()) {
      JFactory::getApplication()->enqueueMessage('Извините произошла ошибка', 'error');
      return false;
    }
    
    JFactory::getApplication()->enqueueMessage('Ваша заявка на уастие в конгрессе принята.');
    // JFactory::getApplication()->enqueueMessage('Оплатить участие в конрессе вы можете в личном кабинете или на странице со списком конгресов нажав кноку оплатить!');
    return true;
  }

  public function check_payment() {

    $secret = 'S4KDHyTSvH4AuimeHP0N6ibsN';

    $dataToSign = array();
    foreach (array(
      'notification_type', 'operation_id', 'amount', 
      'currency', 'datetime', 'sender', 'codepro', 
      'notification_secret', 'label'
    ) as $key) {
      $dataToSign[$key] = $key == 'notification_secret' ? $secret : $_POST[$key];
    }
    $dataToSign['amount'] = 645;
    $sha1_hash = sha1(implode('&', $dataToSign));

    // если тру записываем платеж
    return $sha1_hash == $_POST['sha1_hash'];

  }

  public function member_pay( $user, $con_id ) {
    // создаём уведомление
    $notification = new YandexNotification();
    // указываем параметры
    $notification->codepro = false;
    $notification->label = 53243;
    // отправляем уведомления на ваш сервер
    $notification->dispatch(
        'http://urologists/registratsiya-k?task=registration.check', 
        'S4KDHyTSvH4AuimeHP0N6ibsN' // уведомление будет подписано указанным секретом
    );

    $cmodel = JModelLegacy::getInstance('Congresses', 'CongressModel');
    $reg_row = $cmodel->getRegisterByArgs($con_id, $user->id);
    
    $table = $this->getTable();

    $data = array(
      'id'          => $reg_row->id,
      'user_id'     => $user->id,
      'congress_id' => $con_id,
      'payment'     => 634.0
    );

    if (!$table->bind($data)) {
      JFactory::getApplication()->enqueueMessage('Извините произошла ошибка', 'error');
      return false;
    }

    if (!$table->store()) {
      JFactory::getApplication()->enqueueMessage('Извините произошла ошибка', 'error');
      return false;
    }

    JFactory::getApplication()->enqueueMessage('Оплата произведена успешно!');
    return true;
  }

  public function check_user_registration($user_id) {
    if (!isset($user_id) || empty($user_id))
      return true;

    $db = $this->getDbo();
    $query = $db->getQuery(true);
    $table = $this->getTable();

    $query->select($db->quoteName('id'));
    $query->from($db->quoteName('#__uror_registrationsn'));
    $query->where($db->quoteName('user_id') . ' = "' . $user_id . '"');

    $db->setQuery($query);
    
    if ($user = $db->loadRow()) {
      return true;
    } else {
      return false;
    }
  }

}